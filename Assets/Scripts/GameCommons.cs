﻿using UnityEngine;
using System.Collections;

public class GameCommons {

    public int scoreP1 { get; set; }
    public int scoreP2 { get; set; }

    public GameCommons()
    {
        this.scoreP1 = 0;
        this.scoreP2 = 0;
    }
}
