﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

	public float speed = 25; 
	private float y;
	private Vector2 direction;
    
    private int scoreP1, scoreP2;
    private string scorer;
    private string colliderName;
    public GUISkin layout;

    public Font myFont;

    private GameCommons commons = new GameCommons();

	// Use this for initialization
	void Start () {
        scorer = "P1";
        scoreP1 = commons.scoreP1;
        scoreP2 = commons.scoreP2;
	}

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if(scorer.Equals("P1")) {
                GetComponent<Rigidbody2D>().velocity = Vector2.right * speed;
            } else if(scorer.Equals("P2")) {
                GetComponent<Rigidbody2D>().velocity = Vector2.left * speed;
            }
        }
    }

	// Note: 'collision' holds the collision information.
	// If the Ball collided with a racket, then:
	//   col.gameObject is the racket
	//   col.transform.position is the racket's position
	//   col.collider is the racket's collider
	void OnCollisionEnter2D(Collision2D collision) {

        colliderName = collision.gameObject.name;

		// Hit the left Racket?
        if (colliderName.Equals("RacketLeft"))
        {
			// Calculate hit Factor
			y = hitFactor (transform.position,
			               collision.transform.position,
			               collision.collider.bounds.size.y);

			// Calculate direction, make length=1 via .normalized
			direction = new Vector2(1, y).normalized;
			// Set Velocity with dir * speed
			GetComponent<Rigidbody2D>().velocity = direction * speed;
		}

        if (colliderName.Equals("RacketRight"))
        {
			// Calculate hit Factor
			y = hitFactor (transform.position,
			               collision.transform.position,
			               collision.collider.bounds.size.y);
			
			// Calculate direction, make length=1 via .normalized
			direction = new Vector2(-1, y).normalized;
			// Set Velocity with dir * speed
			GetComponent<Rigidbody2D>().velocity = direction * speed;
		}

        //otherCollider = collision.contacts[0].otherCollider.name;

        if (colliderName.Equals("WallRight"))
        {
            scoreP1 = ++commons.scoreP1;
            scorer = "P1";
            resetBall("P1");
        }

        if (colliderName.Equals("WallLeft"))
        {
            scoreP2 = ++commons.scoreP2;
            scorer = "P2";
            resetBall("P2");
        }


	}

    void OnGUI()
    {
        GUI.skin = layout;
        GUI.skin.font = myFont;
        GUI.Label(new Rect(Screen.width / 3, 500, 500, 100), "<size=20>Press RETURN to PONG!</size>");
        GUI.Label(new Rect(Screen.width / 2 - 70, 220, 100, 100), "<size=25>" + "" + scoreP1 + "</size>");
        GUI.Label(new Rect(Screen.width / 2 + 50, 220, 100, 100), "<size=25>" + "" + scoreP2 + "</size>");
    }

	// util
	float hitFactor(Vector2 ballPosition,
	                Vector2 racketPosition,
	                float racketHeight) {
		// ||  1 <- at the top of the racket
		// ||  0 <- at the middle of the racket
		// || -1 <- at the bottom of the racket
		return (ballPosition.y - racketPosition.y) / racketHeight;
	}

    void resetBall(string scorer)
    {
        if (scorer.Equals("P1"))
        {
            GetComponent<Rigidbody2D>().transform.position = new Vector2(-3, 0);
        }
        else if (scorer.Equals("P2"))
        {
            GetComponent<Rigidbody2D>().transform.position = new Vector2(3, 0);
        }

        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
    }

    void checkWinner()
    {
        if (scoreP1 == 10)
        {

        }
        else if (scoreP2 == 10)
        {

        }
    }

    void showWinner()
    {

    }
}
