﻿using UnityEngine;
using System.Collections;

public class MoveRacket : MonoBehaviour {

	private float v;

	public float speed = 30;
	public string axis = "VerticalP1";

	void FixedUpdate() {
		v = Input.GetAxisRaw (axis);
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, v) * speed;
	}
}
